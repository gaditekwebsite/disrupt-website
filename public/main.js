// import './style.css'
// import javascriptLogo from './javascript.svg'
// import viteLogo from '/vite.svg'
// import { setupCounter } from './counter.js'

// document.querySelector('#app').innerHTML = `
//   <div>
//     <a href="https://vitejs.dev" target="_blank">
//       <img src="${viteLogo}" class="logo" alt="Vite logo" />
//     </a>
//     <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
//       <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
//     </a>
//     <h1>Hello Vite!</h1>
//     <div class="card">
//       <button id="counter" type="button"></button>
//     </div>
//     <p class="read-the-docs">
//       Click on the Vite logo to learn more
//     </p>
//   </div>
// `

// setupCounter(document.querySelector('#counter'))

$(document).ready(function () {
  $("#seeMore").click(function () {
    $(this).hide();
    $(".portfolioFounder").toggle();
  });

  var swiper = new Swiper(".mySwiper", {
    slidesPerView: 3,
    spaceBetween: 30,
    loop: true,
    centeredSlides: false,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    breakpoints: {
      // when window width is >= 640px
      320: {
        slidesPerView: 1.4,
        spaceBetween: 40,
      },
      // when window width is >= 768px
      768: {
        slidesPerView: 3.1,
        spaceBetween: 20,
      },
      // when window width is >= 1024px
      1024: {
        slidesPerView:5,
        spaceBetween: 10,
      },

    },
    autoplay: {
      delay: 2200,
      disableOnInteraction: false,
    },
  });




    $(".hoverhere").hover(
        function () {
            $(this).find(".changebgtored").css("background-color", "#2B388F");
        },
        function () {
            // Mouse leave: Revert to the original background color
            $(this).find(".changebgtored").css("background-color", "#80808033");
        }
    );
});
