/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./*.js",        // Include all JavaScript files in the root directory
    "./public/*.css" // Include CSS files in the public folder (if needed)
  ],
  theme: {
    extend: {
        screens: {
            'Mobile': {'min': '320px', 'max': '767px'},
            'MobileLandscape': {'min': '665px', 'max': '896px'},
            'Tablet': {'min': '768px', 'max': '1024px'},
            'Ipad': {'min': '1024px', 'max': '1139px'},
            'custom': {'min': '1139px', 'max': '1343px'},
            'desktop': "1344px",
        },
    },
  },
  plugins: [],
}
